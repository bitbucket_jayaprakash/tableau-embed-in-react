import React, { Component } from 'react';
import './App.css';
import tableau from 'tableau-api';

class App extends Component {

  initTableau() {
    const vizUrl = "https://public.tableau.com/views/OpportunityTracking/OpportunityTracking?:embed=y&:display_count=yes&:origin=viz_share_link";

    const options = {
      width: window.innerWidth,
      height: window.innerHeight * 0.8,
      onFirstInteractive: () => {
        console.log('embed successful')
      }
    }

    this.viz = new window.tableau.Viz(this.container, vizUrl, options)

  }

  render() {

    return (
      <div>
        <h2 style={{ "padding": "10px", "textAlign": "center" }}>
          Embedding tableau in a react application
        </h2>
      <div ref={c => { this.container = c }}>
      </div>
      </div>
    )
  }

  componentDidMount() {
    this.initTableau()
  }
}

export default App;
